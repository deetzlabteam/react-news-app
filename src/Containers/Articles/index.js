import React, { Component } from 'react';
import ArticlesList from '../../Components/ArticlesList';

class Articles extends Component {
  state = {
    article: []
  }
  componentDidMount(){
    fetch('https://newsapi.org/v2/everything?sources=mtv-news&apiKey=b55e5ddc44af417ea63f5b8f41db0016')
    .then(response => response.json())
    .then(json => console.log({ music: json }))
  }

  render(){
    return (
      <div>
          The length of music array - {this.state.article.length}
          <ArticlesList list= {this.state.article} />
      </div>
    )
  }
}

export default Articles;
