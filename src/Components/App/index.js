import React, { Component } from 'react';
import Intro from '../Intro'
import logo from '../../logo.png';
import Articles from '../../Containers/Articles';
import './App.css';
import 'whatwg-fetch';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">The News of Tomorrow</h1>
        </header>
        <Intro message = "ChimmyChanga" />
        <Articles />
      </div>
    );
  }
}

export default App;
