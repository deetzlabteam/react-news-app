import React from 'react';

const ArticlesList = (props) => {
  return (
    <div>
        <ul>
          {props.list.map(article =>(
            <li>{article.show.name}</li>
          ))}
        </ul>
    </div>
  )
}

export default ArticlesList
